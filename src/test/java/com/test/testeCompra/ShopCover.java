package com.test.testeCompra;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ShopCover {

    private WebDriver driver;
    private WebElement link;
    private Command command;
    private final String url = "http://automationpractice.com/index.php?";
    private final Cart cart;
    private String priceProduct;
    private String priceShipping;
    

    public ShopCover(WebDriver driver) {
        this.driver = driver;
        command = new Command();
        cart = new Cart(driver);
    }
    
    public String getUrl() {
        return url;
    }
    
    //botão checkout modal cover
    public void proceedToCheckout() throws InterruptedException {
    	command.threadSleep(1000);
    	link = command.findByCssSelector("#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div.button-container > a",driver);
    	command.threadSleep(1000);
    	link.click();
    }

    //seleciona o produto na capa e adiciona no carrinho, faz chamada para o metodo dentro do carrinho.
    public void selectProduct(Actions actions) throws InterruptedException{
    	command.threadSleep(1000);
    	link = command.findByCssSelector("#homefeatured > li.ajax_block_product.col-xs-12.col-sm-4.col-md-3.first-in-line.first-item-of-tablet-line.first-item-of-mobile-line",driver);
    	cart.addToCart(link, actions, driver);
    }
}