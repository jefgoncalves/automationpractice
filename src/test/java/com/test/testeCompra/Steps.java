package com.test.testeCompra;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.thedeanda.lorem.LoremIpsum;

public class Steps {

	private final Register cadastro;
	private final ShopCover capaLoja;
	private final Actions actions;
	private final Cart cart;
	private final Login login;
	private LoremIpsum lorem;
	private final Register register;
	private String priceTotal;
	private String andress1;
	private String andress2;
	private String city;
	private String state;


	public Steps(WebDriver driver, DriversBrowsers browsers) {
		login = new Login(driver);
		cadastro = new Register(driver);
		driver.get(login.getUrl());
		new Command(driver);
		actions = new Actions(driver);
		capaLoja = new ShopCover(driver);
		cart = new Cart(driver);
		lorem = new LoremIpsum();
		register = new Register(driver);
		andress1 = new String();
		andress2 = new String();
		city = new String();
		state = new String();
	}

	// Selecionar o produto na capa
	public void selectProduct() throws InterruptedException {
		capaLoja.selectProduct(actions);

	}

	// Clica no botão checkout da modal que abre ao clicar no botão "add to cat" na capa.
	public void proceedToCheckoutCover() throws InterruptedException {
		capaLoja.proceedToCheckout();
	}

	// Clica no botão checkout dentro do carrinho da compra.
	public void proceedToCheckoutCart() throws InterruptedException {
		priceTotal = cart.proceedToCheckoutCart(priceTotal);
	}

	// Preencher o campo "email address" e clica no botão "create an account"
	public void createaccountButton() throws InterruptedException {
		String email = lorem.getEmail();
		if (cadastro.createaccountButton(email) == true) {
			System.out.println("E-mail utilizado no cadastrado foi: " + email);
			
			fillRegister();
		} else {
			System.out.println(
					"Não foi possivel realizar cadastrado. Verifiquei o email utilizado ou os dados inserido.");
		}
	}

	//Realizar o preenchimento da tela de cadasto
	private void fillRegister() {
		register.fillTitle();
		register.fillCustomerFistName(lorem.getName());
		register.fillCustomerLastName(lorem.getLastName());
		register.fillPassword("123456");
		register.fillDays("6");
		register.fillMonths("August");
		register.fillYears("1988");
		register.fillCompany(lorem.getName());
		andress1 = lorem.getWords(3, 5);
		register.fillAddress1(andress1);
//		andress2 = lorem.getWords(2, 5);
//		register.fillAddress2(andress2);
		city = lorem.getCity();
		register.fillCity(city);
		state = lorem.getStateFull();
		register.fillState(state);
		register.fillZipPostcode(lorem.getZipCode());
		register.fillAdditionalInformation(lorem.getWords(2, 5));
		register.fillPhone(lorem.getPhone());
		register.fillPhoneMobile(lorem.getPhone());
		register.registerButton();
	}

	// Clica no botão checkout da tela de endereço.
	public void proceedToCheckoutAddress() throws InterruptedException {
		if(cart.proceedToCheckoutAddress(andress1)==true) {
			System.out.println("Passou para escolher a opção de entrega, e aceitar o termo de serviço ");
		}
		else {
			System.out.println("Compra não realizada");
		}
	}
	// Clica no botão checkout da tela de envio.
	public void proceedToCheckoutShipping() throws InterruptedException {
		cart.proceedToCheckoutShipping();
	}

	// Clica no check de termo de serviço
	public void acceptTermsOfService() throws InterruptedException {
		cart.acceptTermsOfService();
	}

	// Clica no botão para confirmar order de compra e escolher o tipo de pagamento
	public void confirmMyOrderButton(String payment) throws InterruptedException {
		switch (payment) {
		case "bank":
			
		if (cart.paymentBankWireButton(priceTotal) == true) {
			cart.confirmMyOrderButton();
			System.out.println("A compra foi realizada com sucesso");
		} else {
			System.out.println("Compra não foi realizada.");
		}
			break;

		case "check":
			if (cart.paymentCheckButton(priceTotal) == true) {
				cart.confirmMyOrderButton();
				System.out.println("A compra foi realizada com sucesso");
			} else {
				System.out.println("Compra não foi realizada.");
			}
				break;
		}
	}

}
