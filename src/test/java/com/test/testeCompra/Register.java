package com.test.testeCompra;

import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import static org.junit.Assert.assertEquals;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Register {

    private WebDriver driver;
    private WebElement link;
    private Command command;

    public Register(WebDriver driver) {
        this.driver = driver;
        command = new Command();
    }
    
	public boolean createaccountButton(String email) throws InterruptedException {
		command.threadSleep(500);
		link = command.findById("email_create", driver);
		link.sendKeys(email);
		command.printConsole("Adiconar o email para realizar o cadatrado");
		command.threadSleep(500);
		link = command.findById("SubmitCreate", driver);
		link.click();
		command.printConsole("Clicou no botão para criar o cadastrado");
		command.threadSleep(4000);
		
		link = command.findByCssSelector("#noSlide > h1", driver); //busca o elemento com o titulo da pagina para verificar se esta na tela de criação

		String url = link.getText();
		if (url.equalsIgnoreCase("Create an account")) { //Se o titulo da pagina for "create an account" retorno true para poder começa a criação do usuario, qualquer outro erro, ele retorno false.
			System.out.println("Entrou na tela de cadastro");
			System.out.println("Iniciou o cadastrado");
			command.threadSleep(500);
			return true;
		} else { //verificar se o erro é por email já cadastrado ou por email invalido.
			command.threadSleep(500); 
			link = command.findById("create_account_error", driver);
			String phrase = link.getText();
			command.threadSleep(500);
			if (phrase.equalsIgnoreCase(
					"An account using this email address has already been registered. Please enter a valid password or request a new one.")) {
				System.out.println("E-mail cadastrado já foi utilizado");
				return false;
			} else if (phrase.equalsIgnoreCase("Invalid email address.")) {
				System.out.println("E-mail utilizado esta invalido");
				return false;
			}
		}
		return false;
	}
	
	
    //campo title, para escolher mr. ou mrs.
    public void fillTitle() {
    	link = command.findById("id_gender1", driver);
    	link.click();
    }
    
    public void fillCustomerFistName(String customerFirstName) {
    	link = command.findById("customer_firstname", driver);
    	link.sendKeys(customerFirstName);
    }
    public void fillCustomerLastName(String customerLastName) {
    	link = command.findById("customer_lastname", driver);
    	link.sendKeys(customerLastName);
    }
    public void fillPassword(String password) {
    	link = command.findById("passwd", driver);
    	link.sendKeys(password);
    }
    public void fillDays(String days) {
    	link = command.findById("days", driver);
    	link.sendKeys(days);
    }
    public void fillMonths(String months) {
    	link = command.findById("months", driver);
    	link.sendKeys(months);
    }
    public void fillYears(String years) {
    	link = command.findById("years", driver);
    	link.sendKeys(years);
    }
    public void fillFistName(String firstName) {
    	link = command.findById("firstname", driver);
    	link.sendKeys(firstName);
    }
    public void fillLastName(String lastName) {
    	link = command.findById("lastname", driver);
    	link.sendKeys(lastName);
    }
    public void fillCompany(String company) {
    	link = command.findById("company", driver);
    	link.sendKeys(company);
    }
    public void fillAddress1(String address1) {
    	link = command.findById("address1", driver);
    	link.sendKeys(address1);
    }
    public void fillAddress2(String address2) {
    	link = command.findById("address2", driver);
    	link.sendKeys(address2);
    }
    public void fillCity(String city) {
    	link = command.findById("city", driver);
    	link.sendKeys(city);
    }
    public void fillState(String state) {
    	link = command.findById("id_state", driver);
    	link.sendKeys(state);
    }
    public void fillZipPostcode(String postcode) {
    	link = command.findById("postcode", driver);
    	link.sendKeys(postcode);
    }
    public void fillCountry(String country) {
    	link = command.findById("country", driver);
    	link.sendKeys(country);
    }
    public void fillAdditionalInformation(String additionalInformation) {
    	link = command.findById("other", driver);
    	link.sendKeys(additionalInformation);
    }
    public void fillPhone(String phone) {
    	link = command.findById("phone", driver);
    	link.sendKeys(phone);
    }
    public void fillPhoneMobile(String phoneMobile) {
    	link = command.findById("phone_mobile", driver);
    	link.sendKeys(phoneMobile);
    }
    public void fillAssign(String assign) {
    	link = command.findById("alias", driver);
    	link.sendKeys(assign);
    }
    public void registerButton() {
    	link = command.findById("submitAccount", driver);
    	link.click();
    }
   
    
}



