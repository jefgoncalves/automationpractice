package com.test.testeCompra;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.sun.jna.platform.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.Actions;

/**
 * Executa comandos diversos. TODO refatorar.
 *
 * @author TC-JefersonGoncalves
 *
 */
public class Command {

    int timeout = 10;
    private WebDriver driver;
    private Actions actions;

    public Command(WebDriver driver) {
        this.driver = driver;
        actions = new Actions(driver);
    }

    public Command() {

    }

    public void sendKeys(WebElement element, String keys) {
        element.sendKeys(keys);
    }

    public WebElement findByName(String name, WebDriver driver) {
        return driver.findElement(By.name(name));
    }

    public WebElement findByName(String name) {
        return this.driver.findElement(By.name(name));
    }

    public WebElement findById(String name, WebDriver driver) {
        return driver.findElement(By.id(name));
    }

    public WebElement findById(String name) {
        return this.driver.findElement(By.id(name));
    }

    public WebElement findByXpath(String xpath, WebDriver driver) {
        return driver.findElement(By.xpath(xpath));
    }

    public WebElement findByXpath(String xpath) {
        return driver.findElement(By.xpath(xpath));
    }

    public WebElement findByCssSelector(String cssSelector, WebDriver driver) {
        return driver.findElement(By.cssSelector(cssSelector));
    }

    public WebElement findByCssSelector(String cssSelector) {
        return driver.findElement(By.cssSelector(cssSelector));
    }

    public WebElement findByClassName(String name, WebDriver driver) {
        return driver.findElement(By.className(name));
    }

    public WebElement findByClassName(String name) {
        return driver.findElement(By.className(name));
    }

    public void timeoutPagina(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
    }

    public void screenshot(WebDriver driver) throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
// Now you can do whatever you need to do with it, for example copy somewhere
        org.apache.commons.io.FileUtils.copyFile(scrFile, new File("" + Calendar.getInstance().getTimeInMillis() + ".png"));

    }

    public void threadSleep(int tempoMiliSegundos) throws InterruptedException {
        Thread.sleep(tempoMiliSegundos);
    }

    public void threadSleep() throws InterruptedException {
        Thread.sleep(500);
    }

    public void limpaCampo(WebElement element) throws InterruptedException {
        threadSleep();
        element.click();
        element.clear();
        element.sendKeys(Keys.SPACE);
        element.sendKeys(Keys.BACK_SPACE);
    }

    public void preencheCampo(WebElement element, String texto) {
//        this.link = element;
//        element.clear();
        sendKeys(element, texto);
    }


    public void printConsole(String msg) {
        System.out.println(msg);
    }
}
