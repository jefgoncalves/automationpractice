package com.test.testeCompra;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;

/**
 * @author jefersongoncalves
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING) //Realiza os testes conforme a ordem colocada no caso de teste

public class TestsAutomationPractice {

    private WebDriver driver;
    private DriversBrowsers browser;
    private TestCase ct;
    private Steps step;
    private Cart cart;
    private Command command;

    @Before // Abrir o browser e inicializar todos os construtores.
    public void setUp() throws InterruptedException {
        browser = new DriversBrowsers();
        driver = browser.driverChrome();
        driver.manage().window().maximize();
        ct = new TestCase(driver);
        step = new Steps(driver, browser);
        cart = new Cart(driver);
        command = new Command();
        
    }
    
    @Test
    public void selectProduct() throws InterruptedException {
    	ct.compra();
    	
    }
    

    @After //Fechar o driver
    public void SetDown() {
        driver.quit();
    }

}
