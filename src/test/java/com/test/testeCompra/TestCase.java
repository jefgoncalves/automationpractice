package com.test.testeCompra;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

/**
 * @author jefersongoncalves
 */

//Obs.: Para o metodo confirmMyOrderButton, deve-ser escolher o tipo de pagamento "bank" ou "check"

public class TestCase {

    private WebDriver driver;
    private final Steps step;
    private DriversBrowsers browser;
    private final Command command;

    public TestCase(WebDriver driver) {
        this.driver = driver;
        command = new Command(driver);
        step = new Steps(driver, browser);
    }

    //Caso de Teste para realizar compra
    public void compra() throws InterruptedException {
        command.printConsole("------------------------------------------------------------------------------");
        command.printConsole("Caso de teste iniciou.");
        step.selectProduct();
    	step.proceedToCheckoutCover();
    	step.proceedToCheckoutCart();
    	step.createaccountButton();
    	step.proceedToCheckoutAddress();
    	step.acceptTermsOfService();
    	step.proceedToCheckoutShipping();
    	step.confirmMyOrderButton("check"); // bank ou check
    	command.printConsole("Caso de teste finalizou");
        command.printConsole("------------------------------------------------------------------------------");
    };
    
    

}
