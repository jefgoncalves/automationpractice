package com.test.testeCompra;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Cart {

    private WebDriver driver;
    private WebElement link;
    private Command command;

    public Cart(WebDriver driver) {
        this.driver = driver;
        command = new Command();
    }
    
    //Clicar no botão checkout do carinho
    public String proceedToCheckoutCart(String priceTotal) throws InterruptedException {
    
    	command.threadSleep(500);
    	link = command.findByCssSelector("#cart_summary > tfoot", driver); //Pega todos os elementos TD para imprimir
    	command.printConsole("Valores e Taxas");
    	command.printConsole(link.getText()); //Imprimir todos os valores que estão no carrinho (Produto, Shipping e Tax)
    	
        priceTotal = link.findElement(By.id("total_price_container")).getText(); //guarda o valor final para fazer a comparação do valor da compra
    	
    	link = command.findByCssSelector("#center_column > p.cart_navigation.clearfix > a.button.btn.btn-default.standard-checkout.button-medium", driver);
    	command.threadSleep(500);
    	link.click();
     	command.printConsole("Carrinho foi aberto");
     	return priceTotal;
    }
    
    //Faz aparecer o hover e clicar no botão.
    public void addToCart(WebElement element, Actions actions, WebDriver driver) throws InterruptedException {
    	command.threadSleep(1000);
        actions.moveToElement(element).click().build().perform();
        command.threadSleep(1000);
        WebElement element2 = command.findByCssSelector("#homefeatured > li.ajax_block_product.col-xs-12.col-sm-4.col-md-3.first-in-line.first-item-of-tablet-line.first-item-of-mobile-line > div > div.right-block > div.button-container > a.button.ajax_add_to_cart_button.btn.btn-default", driver);
        element2.click();
     	command.printConsole("Escolheu o produto");
    }
    
    //check do botão aceitar serviço
    public void acceptTermsOfService() throws InterruptedException {
    	command.threadSleep(500);
    	link = command.findById("uniform-cgv", driver);
    	link.click();
    	command.printConsole("Clicou eno botão 'aceitar Termo de Serviço'");
    }
    //botão checkout da tela de envio
    public void proceedToCheckoutShipping() throws InterruptedException {
    	command.threadSleep(500);
    	link = command.findByCssSelector("#form > p > button", driver);
    	link.click();
    	command.printConsole("Clicou no botão 'Proceed to checkoout na tela de envio");
    }
    
    //botão pagamento bank wire
	public boolean paymentBankWireButton(String priceTotal) throws InterruptedException {
		command.threadSleep(500);
		if (command.findById("total_price", driver).getText().equalsIgnoreCase(priceTotal) == true) {
			link = command.findByCssSelector("#HOOK_PAYMENT > div:nth-child(1) > div > p > a", driver);
			link.click();
			command.printConsole("Escolheu a forma de pagamento do tipo 'Pay by bank wire'");
			return true;
		} else {
			command.printConsole("Preço que esta no carrinho não é igual ao preço da compra selecionada! Verifique o carrinho.");
			return false;
		}

	}
	
    //botão pagamento check
	public boolean paymentCheckButton(String priceTotal) throws InterruptedException {
		command.threadSleep(500);
		if (command.findById("total_price", driver).getText().equalsIgnoreCase(priceTotal) == true) {
			link = command.findByCssSelector("#HOOK_PAYMENT > div:nth-child(2) > div > p > a", driver);
			link.click();
			command.printConsole("Escolheu a forma de pagamento do tipo 'Pay by Check'");
			return true;
		} else {
			command.printConsole("Preço que esta no carrinho não é igual ao preço da compra selecionada! Verifique o carrinho.");
			return false;
		}

	}
	
    //confirma order de pagamento
    public void confirmMyOrderButton() throws InterruptedException {
    	command.threadSleep(500);
    	link = command.findByCssSelector("#cart_navigation > button", driver);
    	link.click();
    	command.printConsole("Clicou no botão para confirmar order de pagmamento");
    }
    
    //botão checkout do endereço
    public boolean proceedToCheckoutAddress(String andress1) throws InterruptedException {
    	command.threadSleep(500);
    	link = command.findById("address_delivery", driver); //Busca o elemento com todos os dados de entrega
        String temp = link.findElement(By.cssSelector("#address_delivery > li.address_address1.address_address2")).getText();
    	if(temp.equalsIgnoreCase(andress1)) {
    	command.printConsole("Dados de Entrega");
    	command.printConsole("Primeiro Nome e Ultimo Nome: "+ link.findElement(By.cssSelector("#address_delivery > li.address_firstname.address_lastname")).getText());
    	command.printConsole("Endereço Companhia: "+ link.findElement(By.cssSelector("#address_delivery > li.address_company")).getText());
    	command.printConsole("Endereço 1 e Endereço 2: "+ link.findElement(By.cssSelector("#address_delivery > li.address_address1.address_address2")).getText());
    	command.printConsole("Cidade, Estado e CEP: "+ link.findElement(By.cssSelector("#address_delivery > li.address_city.address_state_name.address_postcode")).getText());
    	command.printConsole("Pais: "+ link.findElement(By.cssSelector("#address_delivery > li.address_country_name")).getText());
    	command.threadSleep(500);
    	link = command.findByCssSelector("#center_column > form > p > button", driver); //busca o botão para confirmar o endereço
    	link.click();
    	command.printConsole("Endereço confirmando");
    	command.printConsole("Clicou no botão 'proceed to checkoout' na tela de endereço");
    	return true;
    	}else {
    		command.printConsole("Endereço não foi confirmado, verifique!");
    		return false;
    	}
    }
}
