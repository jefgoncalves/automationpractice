package com.test.testeCompra;

import org.openqa.selenium.WebDriver;

public class Login {

    private final String url = "http://automationpractice.com/index.php?";
    private WebDriver driver;

    public Login(WebDriver driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }
}
