Selenium WeDriver Com Java

Exemplo de Selenium WebDriver com Java para o site: Automation Pratice

Pre requisitos:
Ter o java instalado.
Ter baixado para o computador o Chromedriver.

Passo a passo:
Após ter baixado o chromedriver e necessario escolher o local que deseja deixar
salvo o arquivo do chromedriver, apos a escolha alterar abrir o arquivo "DriverBrowser.java".

Na linha 12, no comando "System.setProperty", colocar o caminho do arquivo do chromedriver.
System.setProperty("webdriver.chrome.driver","projectPath/driver/Chrome/chromedriver");

Para o metodo confirmMyOrderButton, deve-ser escolher o tipo de pagamento "bank" ou "check".

Execute os teste apartir da arquivo "TestsAutomationPractice".